package connectFour;

import connectFour.IConnectFourListener.ObserverType;

/**
 * Superclass for any Connect Four Listener.
 * Main purpose is to be subclassed and share the superclass constructor
 * 
 * @author Dave
 *
 */
public abstract class ConnectFourListener implements IConnectFourListener {

  final protected ConnectFourModel model;
  final private ObserverType observerType;
  
  protected ConnectFourListener(ConnectFourModel model, ObserverType observerType) { 
    this.model = model;
    this.observerType = observerType;
  }
  
  @Override
  public ObserverType getObserverType() {
    return observerType;
  }
  
  public ConnectFourModel getModel() {
    return model;
  }
  
}
