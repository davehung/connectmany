package UnitTests;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.List;

import org.junit.Assert;
import org.junit.Test;

import connectFour.*;

public class ConnectFourModelTest {
  
  private ConnectFourModel baseModel;
  
  public ConnectFourModelTest() {
    baseModel = new ConnectFourModel();
  }
  
  @Test
  public void testConstructor_default() {
    try {
      ConnectFourModel mod = new ConnectFourModel();  
    } catch (Exception e) {
      fail();
    }
  }
  
  @Test
  public void testEquals_Same() {
    ConnectFourModel mod1 = new ConnectFourModel();
    assertEquals(mod1, mod1);
  }

  @Test
  public void testEquals_Different() {
    ConnectFourModel mod1 = new ConnectFourModel();
    ConnectFourModel mod2 = new ConnectFourModel();
    assertNotEquals(mod1, mod2);
  }
  
  @Test
  public void testJoinGame_singleListener() {
    ConnectFourListener listener = new Player(baseModel);
    try {
      listener.getModel().joinGame(listener);
    } catch (Exception e) {
      fail();
    }
  }

  @Test
  public void testJoinGame_MultiListener() {
    ConnectFourListener listener1 = new Player(baseModel);
    ConnectFourListener listener2 = new NonPlayer(baseModel);
    try {
      baseModel.joinGame(listener1);
      baseModel.joinGame(listener2);
    } catch (Exception e) {
      fail();
    }
  }
  
  @Test
  public void testGridDim_width() {
    List<List<String>> grid = baseModel.getGrid();
    assertEquals(baseModel.config.horizontal, grid.size());
  }
  
  @Test(expected = UnsupportedOperationException.class)
  public void testGetGrid_immutableOuter() {
    ConnectFourListener listener = new Player(baseModel);
    List<List<String>> immutableGrid = baseModel.getGrid();
    immutableGrid.add(new ArrayList<String>());
  }
  
  @Test(expected = UnsupportedOperationException.class)
  public void testGetGrid_immutableInner() {
    ConnectFourListener listener = new Player(baseModel);
    List<List<String>> immutableGrid = baseModel.getGrid();
    immutableGrid.get(0).add("listener");
  }

  @Test
  public void testGetGrid_horizontalDimension() { 
    assertEquals(baseModel.config.horizontal, baseModel.getGrid().size());
  }
  
  @Test
  public void testGetGrid_tokenStateAtZero() {
    List<List<String>> state0 = baseModel.getGrid();
    for (List<String> col : state0) {
      assertEquals(0, col.size());
    }
  }
  
  @Test(expected = IllegalStateException.class)
  public void testStart_NoListeners() {
    baseModel.start();
  }
  
  @Test
  public void testGetCurrentMover_nonNull() {
    ConnectFourListener listener = new Player(baseModel);
    baseModel.joinGame(listener);
    baseModel.start();
    //baseModel.start(); // Consider removing currentUser dependency on this call.
    assertNotEquals(null, baseModel.getCurrentMover());
  }
  
  @Test
  public void testGetCurrentMover_firstActive() {
    class InactiveListener extends ConnectFourListener {
      public InactiveListener(ConnectFourModel model) {
        super(model, ObserverType.Passive);
      }
      @Override
      public void gameStarted(String firstPlayer) { }
      @Override
      public String getName() { return "Anonymous on the Fly"; }
      @Override
      public void listenerJoined(boolean isMe, String name) { }
      @Override
      public void moveMade(boolean gameStarted, boolean isMe, String name, int column) { }
      @Override
      public void wonGame(boolean isMe, String name) { }
    }
    ConnectFourListener spectator = new InactiveListener(baseModel);
    ConnectFourListener player1 = new Player(baseModel);
    ConnectFourListener player2 = new NonPlayer(baseModel);
    
    baseModel.joinGame(spectator);
    baseModel.joinGame(player1);
    baseModel.joinGame(player2);
    //baseModel.start();
    assertEquals(player1.getName(), baseModel.getCurrentMover());
  }
  
  @Test
  public void testMadeMove_Success() {
    ConnectFourListener player = new Player(baseModel);
    baseModel.joinGame(player);
    boolean madeMove = baseModel.makeMove(player, 0);
    assertTrue(madeMove);
  }
  
  @Test
  public void testMadeMove_Fail() {
    ConnectFourListener player = new Player(baseModel);
    baseModel.joinGame(player);
    boolean madeMove = baseModel.makeMove(player, baseModel.config.horizontal);
    assertFalse(madeMove);
  }
  
  @Test
  public void testMakeMove_legalSingleMoveSelectedStateCheck() {
    ConnectFourListener player = new Player(baseModel);
    baseModel.joinGame(player);
    boolean madeMove = baseModel.makeMove(player, 0);
    List<List<String>> state = baseModel.getGrid();
    assertEquals(1, state.get(0).size());
  }

  @Test
  public void testMakeMove_legalSingleMoveUnselectedStateCheck() {
    ConnectFourListener player = new Player(baseModel);
    baseModel.joinGame(player);
    boolean madeMove = baseModel.makeMove(player, 0);
    List<List<String>> state = baseModel.getGrid();
    for (int i = 1; i < baseModel.config.horizontal; i++) {
      assertEquals(0, state.get(i).size());
    }
  }

  @Test
  public void testMakemove_illegalSingleStateCheck() {
    List<List<String>> state0 = baseModel.getGrid();
    ConnectFourListener player = new Player(baseModel);
    baseModel.joinGame(player);
    boolean madeMove = baseModel.makeMove(player, baseModel.config.horizontal);
    List<List<String>> state1 = baseModel.getGrid();
    for (int i = 1; i < baseModel.config.horizontal; i++) {
      assertEquals(state0.get(i).size(), state1.get(i).size());
    }
  }

  @Test
  public void testDirection_getUpOpposite() {
    ConnectFourModel.Direction dir = ConnectFourModel.Direction.Up;
    assertEquals(ConnectFourModel.Direction.Down, dir.getOpposite());
  }
  
  @Test
  public void testDirection_getDownOpposite() {
    ConnectFourModel.Direction dir = ConnectFourModel.Direction.Down;
    assertEquals(ConnectFourModel.Direction.Up, dir.getOpposite());
  }  
  
  @Test
  public void testDirection_getLeftOpposite() {
    ConnectFourModel.Direction dir = ConnectFourModel.Direction.Left;
    assertEquals(ConnectFourModel.Direction.Right, dir.getOpposite());
  }
  
  @Test
  public void testDirection_getRightOpposite() {
    ConnectFourModel.Direction dir = ConnectFourModel.Direction.Right;
    assertEquals(ConnectFourModel.Direction.Left, dir.getOpposite());
  }
  
  @Test
  public void testDirection_getNOpOpposite() {
    ConnectFourModel.Direction dir = ConnectFourModel.Direction.NoOp;
    assertEquals(ConnectFourModel.Direction.NoOp, dir.getOpposite());
  }
}
