package connectFour;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

final public class ConnectFourDriver {
  
  final private static String execName = "Connect Four";
  
  public static void main(String[] args) {
 
    if (args.length != 2) {
      System.out.println(execName + " <player1> <player2>");
      return;
    }

    // this needs to be deprecated to more general
    if (args[0].equals(args[1])) {
      System.out.println("<player1> and <player2> must be unique names!");
    }
    ConnectFourModel engine = new ConnectFourModel();
    
    for (int i = 0; i < args.length; i++) {
      ConnectFourListener player = new Player(engine, args[i]);
      engine.joinGame(player);
    }
    
    engine.start();
  }

}
