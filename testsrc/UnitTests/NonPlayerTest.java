package UnitTests;

import static org.junit.Assert.*;

import org.junit.Test;

import connectFour.ConnectFourModel;
import connectFour.NonPlayer;
import connectFour.Player;
import connectFour.IConnectFourListener.ObserverType;

public class NonPlayerTest {

  final private ConnectFourModel baseModel;
  
  public NonPlayerTest() {
    baseModel = new ConnectFourModel();
  }
  
  @Test
  public void testConstructor_passModel() {
    try {
      NonPlayer listener = new NonPlayer(baseModel);
    } catch (Exception e) {
      fail();  
    }
  }

  @Test
  public void testConstructor_sameModel() {
    NonPlayer listener = new NonPlayer(baseModel);
    assertEquals(baseModel, listener.getModel());
  }
  
  @Test
  public void testGetObserverType() {
    NonPlayer player = new NonPlayer(baseModel);
    assertEquals(ObserverType.Active, player.getObserverType());
  }

}
