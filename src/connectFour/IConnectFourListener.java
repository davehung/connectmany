package connectFour;

/**
 * Generate listenerTokens on the client side. Those will be used for 
 * checking if it was you who made the call. 
 * 
 * These tokens need to be passed to the model. They guarantee security 
 * for any public calls made to the model.
 * @author Dave
 * 
 */
public interface IConnectFourListener {

    /**
     * This provides context about the listener to the model: 
     * 
     *  Active : indicates that the listener will be making moves;
     *  Passive : indicates that the listener will not be electing move. 
     *    Or equivalently, the elected moves can be ignored.
     *    
     * @author Dave
     *
     */
    public enum ObserverType {Active, Passive}
    
    public ObserverType getObserverType();
    
    public String getName();
    
    public void gameStarted(String firstPlayer);
    
    public void listenerJoined(boolean isMe, String name);
    
    public void moveMade(boolean gameStarted, boolean isMe, String name, int column);
    
    public void wonGame(boolean isMe, String name);
}
