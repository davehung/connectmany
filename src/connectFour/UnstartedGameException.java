package connectFour;

//We may not need this
public class UnstartedGameException extends RuntimeException {

  public UnstartedGameException() { 
    super();
  }

  public UnstartedGameException(String message) { 
    super(message); 
  }
}

