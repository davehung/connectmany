package connectFour;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JButton;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextArea;

import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

final public class Player extends ConnectFourListener {

  final public static String Default_Handle = "Anonymous Player";
  
  final private String name;
  final private JTextArea textArea = new JTextArea();
  final private JTable grid;
  final private int[] columnHeight;
  final private Object lock = new Object();
  
  public Player(ConnectFourModel model, String playerName) {
    super(model, ObserverType.Active);
    name = playerName;
    grid = intializeGrid();
    columnHeight = new int[model.config.horizontal];
    for (int i = 0; i < columnHeight.length; i++) {
      columnHeight[i] = 0;
    }
    initRenderSwing();
  }

  private void initRenderSwing() {
    JFrame frame = new JFrame("Connect Four");
    
    JPanel panel = new JPanel();
    panel.setLayout(new BorderLayout());
    
    textArea.append("Welcome to Connect Four.\n");

    frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    
    panel.add(new JScrollPane(grid), BorderLayout.WEST);
    panel.add(textArea, BorderLayout.CENTER);
    JPanel buttonPanel = new JPanel();

    for (int i = 0; i < model.config.horizontal; i++) {
      ActionListener buttonHandler = new SubmitMoveHandler(this, i);
      JButton button = new JButton("Move " + i);
      button.addActionListener(buttonHandler);
      buttonPanel.add(button);
    }
    
    panel.add(buttonPanel, BorderLayout.SOUTH);
    frame.getContentPane().add(panel);
    frame.setSize(800, 400);
    frame.setVisible(true);
  }
  
  private JTable intializeGrid() {
    String[] columnNames = new String[model.config.horizontal];
    Object[][] tempGrid = new Object[model.config.vertical][model.config.horizontal];
    for (int i = 0; i < model.config.horizontal; i++) {
      columnNames[i] = "Column " + i;
    }
    return new JTable(tempGrid, columnNames);
  }
  
  public Player(ConnectFourModel model) {
    this(model, Default_Handle);
  }

  @Override
  public String getName() {
    return name;
  }

  @Override
  public void gameStarted(String firstPlayer) {
    textArea.append("The Game has begun!\n------------\n");
    textArea.append(firstPlayer.equals(name) ? "It's your Turn.\n" : "");
  }

  @Override
  public void listenerJoined(boolean isMe, String name) {
    textArea.append(isMe? "" : (name + " joined the game.\n"));
  }

  @Override
  public void moveMade(boolean gameStarted, boolean isMe, String name, int column) {
    if (gameStarted == false) {
      textArea.append("Please Wait. The game hasn't started.\n");
    } else {
      updateGrid(name, column);
    }
  }
  private void updateGrid(String name, int column) {
    synchronized(lock) {
      grid.setValueAt(name, model.config.vertical - (columnHeight[column] + 1), column);
      columnHeight[column]++;
    }
  }
  
  @Override
  public void wonGame(boolean isMe, String name) {
    textArea.append("You " + (isMe ? "Win!\n" : "lose.\n"));
  }
  
  private class SubmitMoveHandler implements ActionListener {
    final private Player currentPlayer;
    final private int associatedColumn;
    private SubmitMoveHandler(Player currentPlayer, int associatedColumn) {
      this.currentPlayer = currentPlayer;
      this.associatedColumn = associatedColumn;
    }

    @Override
    public void actionPerformed(ActionEvent e) {
      boolean madeMove = currentPlayer.model.makeMove(currentPlayer, associatedColumn);
      if (madeMove == false) {
        textArea.append(
            model.getCurrentMover().equals(currentPlayer.getName()) == false ? 
            "It's not your turn.\n" : "You can't move here.\n");
        
      }
    }
  }

}
