package UnitTests;

import static org.junit.Assert.*;

import org.junit.Test;

import connectFour.ConnectFourModel;
import connectFour.Player;
import connectFour.IConnectFourListener.ObserverType;

public class PlayerTest {

  final private ConnectFourModel baseModel;
  
  public PlayerTest() {
    baseModel = new ConnectFourModel();
  }
  
  @Test
  public void testConstructor_passModel() {
    try {
      Player listener = new Player(baseModel);
    } catch (Exception e) {
      fail();  
    }
  }

  @Test
  public void testConstructor_sameModel() {
    Player listener = new Player(baseModel);
    assertEquals(baseModel, listener.getModel());
  }
  
  @Test
  public void testConstructor_anonName() {
    Player listener = new Player(baseModel);
    assertEquals(Player.Default_Handle, listener.getName());
  }

  @Test
  public void testConstructor_userName() {
    String user = "user";
    Player listener = new Player(baseModel, user);
    assertEquals(user, listener.getName());
  }
 
  @Test
  public void testGetObserverType() {
    Player player = new Player(baseModel);
    assertEquals(ObserverType.Active, player.getObserverType());
  }
  
}
