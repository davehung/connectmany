package connectFour;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import connectFour.IConnectFourListener.ObserverType;

/**
 * Extend customization:
 * Allow users public access to the ConnectFourModel.ConnectFourConfig.
 * Also consider other constructor patterns (builder is recommended)
 * 
 * The following constructor method is also recommended for extension:
 * public ConnectFourModel(ConnectFourConfig customeConfig) { ... }
 * @author Dave
 *
 */
final public class ConnectFourModel {
  
  final public ConnectFourConfig config; 
  final private IGridCommunicator<List<List<IConnectFourListener>>> grid;
  final private IListeners listeners = new LinkedListener();
  final private Object gameLock = new Object(); 
  private boolean gameLive = false;
  
  /**
   * The ConnectFourConfig class is intended to store immutable configurations
   * for each instance of ConnectFourModel.
   * 
   * The members are exposed, so public access can read the fields, but extension
   * requires immutability enforcement.
   * 
   * Consider a factory method.
   * 
   * Consider builder pattern to set config parameters.
   * @author Dave
   *
   */
  final public class ConnectFourConfig {
    // TODO implement builder here
    final public int vertical; 
    final public int horizontal;
    final public int goal;
    
    public ConnectFourConfig() {
      vertical = 6;
      horizontal = 7;
      goal = 4;
    }
  }
  
  /**
   * This interface protects the private implementation of the grid
   * from the ConnectFourModel calling methods. This provides and API
   * to the ConnectFourModel.
   * @author Dave
   *  
   * @param <T> 
   * The exposed implementation type of a Connect Four grid. 
   *  We intend for Listeners to resolve the grid of type T.
   *  It is also stressed that IGridCommunicator should make no assumptions
   *  about business logic.
   */
  private interface IGridCommunicator<T> {

    /**
     * Returns the Connect Four grid represented by 
     * an immutable data structure of type T.
     * @return
     */
    public T getGridProtected();

    /**
     * @param columnSelection
     * Connect Four model adds a IConnectFourListener token to column columnSelection 
     * @param mover
     * Connect Four model indicates to IGridCommunicator<T> which IConnectFourListener 
     *  token to place into the grid.
     */
    public boolean addMoveToken(IConnectFourListener mover, int columnSelection);
  }
  
  /**
   * Responsible for maintaining the state of the game.
   * Provides convenience methods to the model.
   * 
   * The model is responsible for enforcing any business logic.
   * 
   * This class is only accessible by ConnectFourModel
   * Critical: private void updateProtectedGridCache()  
   * @author Dave
   *
   */
  final private class ConnectFourGrid implements 
    IGridCommunicator<List<List<IConnectFourListener>>> {

    /**
     * The main 2 dimensional data structure for tracking game state 
     * for an N(columns) X M(rows) grid.
     * 
     * [0,M] ..  [N,M]
     * .          .
     * .          .
     * [0,0] ..  [N,0]
     * 
     * We also maintain a mirrored data structure that just caches 
     * a copy of the internal grid
     */
    final private List<List<IConnectFourListener>> gridInternal;
    private List<List<IConnectFourListener>> protectedGridCache;
    
    /** **CRITICAL**
     * must call updateProtectedGridCache()
     * @param config
     */
    private ConnectFourGrid(ConnectFourConfig config) {
      List<List<IConnectFourListener>> setupList = 
          new ArrayList<List<IConnectFourListener>>();
      for (int column = 0; column < config.horizontal; column++) {
        setupList.add(new ArrayList<IConnectFourListener>());
      }
      gridInternal = Collections.unmodifiableList(setupList);
      updateProtectedGridCache();
    }
    
    /**
     * **CRITICAL**
     * Update the immutable grid cache for performance purposes
     * This method must be called by any state modifications post-modification.
     */
    private void updateProtectedGridCache() {
      protectedGridCache = new ArrayList<List<IConnectFourListener>>();
      for (List<IConnectFourListener> unProtectedColumn : gridInternal) {
        protectedGridCache.add(Collections.unmodifiableList(unProtectedColumn));
      }
      protectedGridCache = Collections.unmodifiableList(protectedGridCache);
    }
    
    @Override
    public List<List<IConnectFourListener>> getGridProtected() {
      return protectedGridCache;
    }
    
    /* 
     * **CRITICAL**
     * updateProtectedGridCache() must be called when the state is updated
     * @see connectFour.ConnectFourModel.IGridCommunicator#addMoveToken(connectFour.IConnectFourListener, int)
     */
    @Override
    public boolean addMoveToken(IConnectFourListener mover, int columnSelection) {
      boolean success = gridInternal.get(columnSelection).add(mover);
      updateProtectedGridCache();
      return success;
    }
  }
    
  /**
   * This interface protects the private implementation of the listeners
   * from model's access.
   * @author Dave
   *
   */
  private interface IListeners {
    public boolean add(IConnectFourListener element);
    public IConnectFourListener getCurrentPlayer();
    public List<IConnectFourListener> getListeners();
    public List<IConnectFourListener> getActiveListeners();
    public boolean contains(IConnectFourListener newListener);
    public void nextTurn();
  }
  
  /**
   * Maintains a linked chain of active players, intended to maintain the 
   * state of the current active player.
   * @author Dave
   *
   */
  final private class LinkedListener implements IListeners {

    final List<IConnectFourListener> listeners = new ArrayList<IConnectFourListener>();
    final List<IConnectFourListener> activeListeners = new ArrayList<IConnectFourListener>();
    private PlayerLink previousPlayerLink;
    private PlayerLink currentPlayerLink;
    private PlayerLink priorCurrentLink;
    final private Object lock = new Object();
  
    public boolean contains(IConnectFourListener elem) {
      boolean isContained = listeners.contains(elem);
      return isContained;
    }
    
    /* 
     * Reject duplicate listeners.
     * @see connectFour.ConnectFourModel.IListeners#add(connectFour.IConnectFourListener)
     */
    @Override
    public boolean add(IConnectFourListener element) {
      if (!this.contains(element)) {
        if (element.getObserverType() == ObserverType.Active) {
          addPlayerLink(element);
        }
        boolean success = listeners.add(element);
        if (success == true) {
          activeListeners.add(element);
        }
        return success;
      } else {
        return false;
      }
    }
    
    private void addPlayerLink(IConnectFourListener newActivePlayer) {
      if (previousPlayerLink == null) {
        previousPlayerLink = new PlayerLink(newActivePlayer);
        previousPlayerLink.cdr = previousPlayerLink;
        priorCurrentLink = previousPlayerLink.cdr;
        currentPlayerLink = priorCurrentLink.cdr;
      } else {
        priorCurrentLink.cdr = new PlayerLink(newActivePlayer, currentPlayerLink);
        priorCurrentLink = previousPlayerLink.cdr;
        currentPlayerLink = priorCurrentLink.cdr;
      }
    }

    @Override
    public IConnectFourListener getCurrentPlayer() {
      return currentPlayerLink == null ? null : currentPlayerLink.car;
    }
    
    @Override
    public List<IConnectFourListener> getListeners() {
      return Collections.unmodifiableList(listeners);
    }
    
    @Override
    public List<IConnectFourListener> getActiveListeners() {
      return Collections.unmodifiableList(activeListeners);
    }

    final private class PlayerLink {

      final private IConnectFourListener car;
      private PlayerLink cdr;

      private PlayerLink(IConnectFourListener car) {
        this.car = car;
        this.cdr = null;
      }
      
      private PlayerLink(IConnectFourListener car, PlayerLink cdr) {
        this.car = car;
        this.cdr = cdr;
      }
    }

    @Override
    public void nextTurn() {
      synchronized(lock) {
        if (currentPlayerLink == null) {
          throw new IllegalStateException("No active players!"); 
        }
        do {
          previousPlayerLink = previousPlayerLink.cdr;
          priorCurrentLink = priorCurrentLink.cdr; 
          currentPlayerLink = currentPlayerLink.cdr;
        } while (currentPlayerLink.car.getObserverType() != ObserverType.Active);
      }
    }
    
  }

  public ConnectFourModel() {
    config = new ConnectFourConfig();
    grid = new ConnectFourGrid(config);
  }
    
  /**
   * Only a single instance of IConnectFourListener can listen to the model.
   * This prevents cheating when a single player attempts to take two moves 
   * in the same turn. This also prevents double post backing to Passive 
   * listeners
   * 
   * @param newListener
   */
  public void joinGame(IConnectFourListener newListener) {
    if (listeners.contains(newListener)) {
      for (IConnectFourListener activeListener : listeners.getActiveListeners()) {
        if (activeListener.getName().equals(newListener.getName())) {
          throw new IllegalStateException("Name must be unique!");
        }
      }
      // reject the request to join
      return;
    } else {
      listeners.add(newListener);
      fireListenerJoinedEvent(newListener);
    }
  }
  private void fireListenerJoinedEvent(IConnectFourListener joinedListener) {
    for (IConnectFourListener listener : listeners.getListeners() ) {
      listener.listenerJoined(listener.equals(joinedListener), listener.getName());
    }
  }

  public List<List<String>> getGrid() {
    List<List<IConnectFourListener>> protectedGrid = grid.getGridProtected();
    List<List<String>> output = new ArrayList<List<String>>();
    for (List<IConnectFourListener> innerColumn : protectedGrid) {
      List<String> newColumn = new ArrayList<String>();
      for (IConnectFourListener piece : innerColumn) {
        newColumn.add(piece.getName());
      }
      output.add(Collections.unmodifiableList(newColumn));
    }
    return Collections.unmodifiableList(output);
  }
  
  public void start() {
    synchronized(gameLock) {
      if (gameLive) {
        // never initialize the game more than once
        return;
      } else {
        gameLive = true;
        if (listeners.getCurrentPlayer() == null) {
          throw new IllegalStateException("No active players!");
        }
        fireGameStartedEvent();
      }
    }
  }
  private void fireGameStartedEvent() {
    for (IConnectFourListener currentListener : listeners.getListeners() ) {
      currentListener.gameStarted(listeners.getCurrentPlayer().getName());
    }
  }

  /**
   * Returns the name of the player whose turn it is.
   * @return
   */
  public String getCurrentMover() {
    return listeners.getCurrentPlayer().getName();
  }
  
  public boolean makeMove(IConnectFourListener requester, int chooseColumn) {
    if (chooseColumn >= 0 
        && chooseColumn < config.horizontal
        && requester.equals(listeners.getCurrentPlayer())
        && grid.getGridProtected().get(chooseColumn).size() < config.vertical) {
      boolean success = grid.addMoveToken(requester, chooseColumn);
      if (success == true) {
        listeners.nextTurn();
        fireMoveMadeEvent(requester, chooseColumn);
        checkWinner(requester, chooseColumn); 
      }
      return success; 
    } else {
      return false;
    }
  }
  private void fireMoveMadeEvent(IConnectFourListener mover, int chooseColumn) {
    for (IConnectFourListener listener : listeners.getListeners()) {
      listener.moveMade(gameLive, listener == mover, mover.getName(), chooseColumn);
    }
  }
  private void checkWinner(IConnectFourListener mover, int lastMove) {
    int currentRow = grid.getGridProtected().get(lastMove).size() - 1;
    int horizNeighbors = countConnectedNeighbors(mover, lastMove, currentRow,
        ConnectFourModel.Direction.Right, ConnectFourModel.Direction.NoOp);
    int vertNeighbors = countConnectedNeighbors(mover, lastMove, currentRow,
        ConnectFourModel.Direction.NoOp, ConnectFourModel.Direction.Up);
    int diagUpNeighbors = countConnectedNeighbors(mover, lastMove, currentRow,
        ConnectFourModel.Direction.Right, ConnectFourModel.Direction.Up);
    int diagDownNeighbors = countConnectedNeighbors(mover, lastMove, currentRow,
        ConnectFourModel.Direction.Right, ConnectFourModel.Direction.Down);
    if (horizNeighbors >= config.goal 
        || vertNeighbors >= config.goal
        || diagUpNeighbors >= config.goal
        || diagDownNeighbors >= config.goal) {
      fireWonGameEvent(mover);
    }
  }
  private void fireWonGameEvent(IConnectFourListener winner) {
    for (IConnectFourListener listener : listeners.getListeners()) {
      listener.wonGame(listener == winner, winner.getName());
    }
  }
  /**
   * pass the four cardinal directions:
   *  vertical, 
   *  horizontal, 
   *  diagonal up,
   *  diagonal down
   * @param checkMe
   * @param columnIndex
   * @param rowIndex
   * @param horizontal
   * @param vertical
   * @return
   */
  private int countConnectedNeighbors(IConnectFourListener checkMe,
      int columnIndex, 
      int rowIndex,
      ConnectFourModel.Direction horizontal, 
      ConnectFourModel.Direction vertical) {
    /* 
     *  
     */
    int cardinalDirectionCount = countDirectedConnectedNeighbors(checkMe,
        columnIndex,
        rowIndex,
        horizontal,
        vertical);
    int oppositeDirectionCount = countDirectedConnectedNeighbors(checkMe,
        columnIndex + horizontal.getOpposite().getOffset(),
        rowIndex + vertical.getOpposite().getOffset(),
        horizontal.getOpposite(),
        vertical.getOpposite());
    return cardinalDirectionCount + oppositeDirectionCount;
  }
  private int countDirectedConnectedNeighbors(IConnectFourListener checkMe,
      int columnIndex, 
      int rowIndex,
      ConnectFourModel.Direction horizontal, 
      ConnectFourModel.Direction vertical) {
    List<List<IConnectFourListener>> state = grid.getGridProtected();
    if (columnIndex < 0 || columnIndex >= config.horizontal) {
      return 0;
    }
    List<IConnectFourListener> currentColumn = state.get(columnIndex);
    if (rowIndex < 0 || rowIndex >= currentColumn.size()) {
      return 0;
    }
    if (checkMe != currentColumn.get(rowIndex)) {
      return 0;
    }
    return 1 + countDirectedConnectedNeighbors(checkMe, 
        columnIndex + horizontal.getOffset(),
        rowIndex + vertical.getOffset(),
        horizontal,
        vertical);
    }

  public enum Direction { 
    Left(-1), 
    Right(1), 
    Up(1), 
    Down(-1), 
    NoOp(0);
    
    final private int offset;
    
    Direction(int offset) {
      this.offset = offset;
    }
    
    public int getOffset() {
      return offset;
    }

    public Direction getOpposite() {
      switch (this) {
      case Left : return Right;
      case Right : return Left;
      case Up : return Down;
      case Down : return Up;
      case NoOp : return NoOp;
      }
      return NoOp; 
    }
    
    }
  
  @Override
  public boolean equals(Object o) {
    return this == o;
  }  
}
