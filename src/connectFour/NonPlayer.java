package connectFour;

import connectFour.IConnectFourListener.ObserverType;

final public class NonPlayer extends ConnectFourListener implements
    IConnectFourListener {

  final private static String name = "Computer Player";
  
  public NonPlayer(ConnectFourModel model) {
    super(model, ObserverType.Active);
  }

  @Override
  public String getName() {
    return NonPlayer.name;
  }

  @Override
  public void gameStarted(String firstPlayer) {
    // TODO Auto-generated method stub
  }

  @Override
  public void listenerJoined(boolean isMe, String name) {
    // TODO Auto-generated method stub
  }

  @Override
  public void moveMade(boolean gameStarted, boolean isMe, String name, int column) {
    // TODO Auto-generated method stub
  }

  @Override
  public void wonGame(boolean isMe, String name) {
    // TODO Auto-generated method stub
    
  }
}
